<?php

use MediaWiki\MediaWikiServices;
use Telepedia\TelepediaMagic\TelepediaPageType;

class TelepediaAds {

	/**
	 * Indicates whether the page has ads or not.
	 *
	 * @var bool
	 */
	public static $PAGE_HAS_ADS = false;

	/**
	 * Can we show ads on this page?
	 * @var $wgAdConfig
	 * @global boolean
	 */
	private static function canShowAds( User $user ) {
		global $wgAdConfig, $wgTitle;

		if ( TelepediaPageType::isSpecial() || TelepediaPageType::isFilePage() || TelepediaPageType::isSearch() ) {
			return;
		}
	
		if ( !$wgAdConfig['enabled'] ) {
			return false;
		}
		// @todo: filthy hack, $wgTitle has been depreciated since 1.19;
		$effectiveGroups = MediaWikiServices::getInstance()->getUserGroupManager()->getUserEffectiveGroups( $user );
		if ( $wgTitle instanceof Title && $wgTitle->isSpecial( 'Userlogin' ) || $wgTitle instanceof Title && $wgTitle->isSpecial( 'Recentchanges' ) || $wgTitle instanceof Title && $wgTitle->isSpecial( 'Createaccount' ) || in_array( 'user', $effectiveGroups ) ) {
			return false;
		}
		// if we haven't already exited, we can show advertisements.
		return true;
	}

	/**
	 * Check if we can show ads in the current namespace.
	 * @return false|true
	 */
	public static function isEnabledNamespace() {
		$title = RequestContext::getMain()->getTitle();
		if ( !$title ) {
			return false;
		}
		$namespace = $title->getNamespace();

		if ( !$namespace == 'NS_MAIN' ) {
			return false;
		}

		return true;
	}

	/**
	 * @return string
	 * determine the current skin--since we need to do things differently for mobile
	 */
	public static function determineSkin() {
		global $wgOut;

		return strtolower( $wgOut->getSkin()->getSkinName() );
	}

	public static function getSitenoticeHTML() {
		if ( self::determineSkin() == 'minerva' ) {
			$adHtml = '<!--  [Mobile] Mobile Leaderboard - Above the Fold (ATF) -->
					<div data-pw-mobi="leaderboard_atf" id="pwMobiLbAtf"></div>
					<script type="text/javascript">
					window.ramp.que.push(function () {
						window.ramp.addTag("pwMobiLbAtf");
					})
					</script>';
		} else {
			$adHtml = '<!--  [Desktop/Tablet] Desktop Leaderboard - Above the Fold (ATF) -->
							<div data-pw-desk="leaderboard_atf" id="pwDeskLbAtf"></div>
							<script type="text/javascript">
							window.ramp.que.push(function () {
								window.ramp.addTag("pwDeskLbAtf");
							})
							</script>';
		}
		return '<div class="sitenotice-advertisement">' . $adHtml . '</div>';
	}

	public static function getToolboxHTML() {
		$adHtml = '<!--  [Desktop/Tablet]sky_btf -->
        <div data-pw-desk="sky_btf" id="pwDeskSkyBtf1"></div>
        <script type="text/javascript">
          window.ramp.que.push(function () {
              window.ramp.addTag("pwDeskSkyBtf1");
          })
        </script>';

		return '<div class="toolbox-advertisement" style="max-width: 160px">' . $adHtml . '</div>';
	}

	public static function onSkinAfterPortlet( Skin $skin, $portlet, &$html ) {
		$skins = [ 'vector', 'modern', 'monobook' ];
		$user = $skin->getUser();
		$skin = $skin->getSkinName();

		if (
			in_array( $skin, $skins ) &&
			$portlet === 'tb'
		) {
			$html .= self::getToolboxHTML( $user );
		}
	}

	public static function onBeforePageDisplay( OutputPage $out, Skin $skin ) {
		if ( self::canShowAds( $out->getUser() ) ) {
			self::$PAGE_HAS_ADS = true;
		}

		if ( self::$PAGE_HAS_ADS ) {
			$out->addHeadItem( "ramp", "<script data-cfasync=\"false\">
                              window.ramp = window.ramp || {};
                              window.ramp.que = window.ramp.que || [];
                            </script>
                            <script data-cfasync=\"false\" async src=\"//cdn.intergient.com/1025055/74539/ramp_config.js\"></script>
                                        <script data-cfasync=\"false\">
                                        window._pwGA4PageviewId = ''.concat(Date.now());
                                window.dataLayer = window.dataLayer || [];
                                window.gtag = window.gtag || function () {
                                            dataLayer.push(arguments);
                                        };
                                gtag('js', new Date());
                                gtag('config', 'G-DP0QWXMVB3', { 'send_page_view': false });
                                gtag(
                                    'event',
                                    'ramp_js',
                                  {
                                    'send_to': 'G-DP0QWXMVB3',
                                    'pageview_id': window._pwGA4PageviewId
                                  }
                                );
                            </script><script data-cfasync=\"false\" async src=\"//cdn.intergient.com/ramp_core.js\"></script>" );
		}
	}

	public static function onSkinAfterBottomScripts( $skin, &$text ) {
		global $wgAdConfig;
		if ( self::$PAGE_HAS_ADS ) {
			$text .= '<script data-cfasync="false" async src="//cdn.intergient.com/ramp_core.js"></script>' . "\n";
		}
	}

	public static function onSiteNoticeAfter( &$siteNotice, Skin $skin ) {
		// load the contents of sitenoticehtml in here
		$siteNotice .= self::getSitenoticeHTML();
	}
}
