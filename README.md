# TelepediaAds

**TelepediaAds** is an implementation of ad serving for the Telepedia Platform. We use Playwire at Telepedia and as such, 
this code is heavily customised to our setup—it is not intended to be portable to other wikis. It is based on ShoutWikiAds.

# Security Vulnerabilities

If you believe you have found a security vulnerability in any part of our code, please do not post it publicly by using our wikis or bug trackers for that. Rather, please approach a member of staff directly, or using Phorge in the first instance.

As a quick overview, you can email security concerns to community@telepedia.net which will open a phabricator task that is hidden from public view. If you'd like, you can instead directly create a security-related task [here](https://phorge.telepedia.net), but please leave the "Security" project on the issue.